import "./App.css";

import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Provider } from "react-redux";
import 'bootstrap/dist/css/bootstrap.min.css';

import TeamsContainer from "./components/teams/teamsContainer";
import PlayersContainer from "./components/players/playersContainer";
import store from "./store";

function App() {
  return (
    <Provider store={store}>
      <Router>
        <React.Fragment>
          <Switch>
            <Route path="/" exact component={TeamsContainer} />
            <Route path="/teams/players/:id" component={PlayersContainer} />
          </Switch>
        </React.Fragment>
      </Router>
    </Provider>
  );
}

export default App;
