import React, { Component } from "react";

import { Button, Card, Form } from "react-bootstrap";

class CreatePlayer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      playerName: "",
      playerNumber: "",
      playerAge: "",
      playerType: "",
    };
  }

  handleInputChange = (e) => {
    const target = e.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value,
    });
  };

  render() {
    return (
      <Card
        className="mb-2 my-3"
        style={{
          display: `${this.props.show}`,
          width: "13rem",
          height: "16rem",
        }}
      >
        <Card.Header>ADD YOUR PLAYER</Card.Header>
        <Card.Body>
          <Form
            onSubmit={(e) => {
              e.preventDefault();
              this.props.onCreate(this.state);
            }}
          >
            <Form.Control
              style={{ margin: "8px 0px" }}
              size="sm"
              type="text"
              placeholder="player Name"
              name="playerName"
              value={this.state.playerName}
              onChange={this.handleInputChange}
            />
            <Form.Control
              style={{ margin: "8px 0px" }}
              size="sm"
              type="text"
              placeholder="player Number"
              name="playerNumber"
              value={this.state.playerNumber}
              onChange={this.handleInputChange}
            />
            <Form.Control
              style={{ margin: "8px 0px" }}
              size="sm"
              type="text"
              placeholder="player Age"
              name="playerAge"
              value={this.state.playerAge}
              onChange={this.handleInputChange}
            />
            <Form.Control
              style={{ margin: "8px 0px 0px" }}
              size="sm"
              type="text"
              placeholder="player position"
              name="playerType"
              value={this.state.playerType}
              onChange={this.handleInputChange}
            />
            <div
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "flex-start",
              }}
            >
              <Button type="submit" size="sm" variant="primary">
                add player
              </Button>
              <Button variant="light" onClick={this.props.onHide}>
                x
              </Button>
            </div>
          </Form>
        </Card.Body>
      </Card>
    );
  }
}

export default CreatePlayer;
