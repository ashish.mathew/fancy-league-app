import React, { Component } from "react";

import { connect } from "react-redux";
import { Button, Badge, Modal } from "react-bootstrap";

import "./playersContainers.css";
import Player from "./player";
import CreatePlayer from "./createPlayer";
import Navbar from "../navbar/navbar";
import ground from "../../ground.jpg";

import {
  fetchPlayers,
  addPlayer,
  deletePlayer,
} from "../../actions/playerActions";

class PlayerContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      showResult: false,
      place: 0,
    };
  }

  handleClose = () => this.setState({ show: false });
  handleShow = () => this.setState({ show: true });
  handleCloseResult = () => this.setState({ showResult: false });
  handleShowResult = () => this.setState({ showResult: true });

  componentDidMount() {
    this.props.fetchPlayers(this.props.match.params.id);
  }

  handleAddPlayer = (player) => {
    this.props.addPlayer(this.props.match.params.id, player);
    this.handleClose();
  };

  handleDeletePlayer = (playerId) => {
    this.props.deletePlayer(playerId);
  };
  handlePlay = () => {
    const place = Math.floor(
      Math.random() * (this.props.teams.length - 1 + 1) + 1
    );
    console.log(place);
    this.setState({ place });
    this.handleShowResult();
  };

  render() {
    return (
      <div>
        <Navbar />
        <Button variant="success" style={{ marginLeft: "10%" }}>
          Players{" "}
          <Badge pill bg="warning">
            {this.props.players.length}
          </Badge>
        </Button>
        {this.state.showResult ? (
          <Modal centered show={this.state.showResult}>
            <Modal.Header>
              <Modal.Title style={{ margin: "0px auto" }}>
                <img
                  alt=""
                  src="https://img.icons8.com/external-vitaliy-gorbachev-flat-vitaly-gorbachev/58/000000/external-cup-back-to-school-vitaliy-gorbachev-flat-vitaly-gorbachev.png"
                />
              </Modal.Title>
            </Modal.Header>
            <Modal.Body style={{ margin: "0px auto" }}>
              <h3>you won {this.state.place} place</h3>
              <span> cach won:{Math.floor(10000 / this.state.place)}</span>
            </Modal.Body>
            <Modal.Footer>
              <Button onClick={this.handleCloseResult}>Close</Button>
            </Modal.Footer>
          </Modal>
        ) : (
          <Button
            variant="light"
            style={{ margin: " 0px 30px" }}
            onClick={this.handlePlay}
          >
            play{" "}
            <img
              alt=""
              src="https://img.icons8.com/fluency/40/000000/play-games.png"
            />
          </Button>
        )}

        <div
          className="player-list"
          style={{
            backgroundImage: `url(${ground})`,
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
            height:"100vh"
          }}
        >
          {this.props.players.map((player) => (
            <Player
              player={player}
              key={player.id}
              onDelete={this.handleDeletePlayer}
            />
          ))}
          <div>
            {this.state.show ? (
              <CreatePlayer
                style={{ width: "14rem", height: "13rem" }}
                show={this.state.show}
                onHide={this.handleClose}
                onCreate={this.handleAddPlayer}
              />
            ) : (
              <Button
                className="my-5"
                style={{
                  width: "13rem",
                  height: "13rem",
                  opacity: "0.6",
                }}
                variant="secondary"
                onClick={this.handleShow}
              >
                <h2>add Player</h2>
              </Button>
            )}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  players: state.players.players,
  teams: state.teams.teams,
});

export default connect(mapStateToProps, {
  fetchPlayers,
  addPlayer,
  deletePlayer,
})(PlayerContainer);
