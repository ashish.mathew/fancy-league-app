import React, { Component } from "react";

import { Button, Card, Form } from "react-bootstrap";

class UpdatePlayer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      player_name: this.props.player.player_name,
      player_number: this.props.player.player_number,
      player_age: this.props.player.player_age,
      player_type: this.props.player.player_type,
      player_image: this.props.player.player_image,
      id: this.props.player.id,
    };
  }

  handleInputChange = (e) => {
    const target = e.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value,
    });
  };

  render() {
    return (
      <Card
        className="mb-2"
        style={{
          display: `${this.props.show}`,
          width: "13rem",
          height: "16rem",
        }}
      >
        <Card.Header>UPDATE YOUR PLAYER</Card.Header>
        <Card.Img variant="top" src={`${this.props.player.player_image}`} />
        <Card.Body>
          <Form
            onSubmit={(e) => {
              e.preventDefault();
              this.props.onUpdate(this.state);
            }}
          >
            <Form.Control
              style={{ margin: "8px 0px" }}
              size="sm"
              type="text"
              placeholder="player Name"
              name="player_name"
              value={this.state.player_name}
              onChange={this.handleInputChange}
            />
            <Form.Control
              style={{ margin: "8px 0px" }}
              size="sm"
              type="text"
              placeholder="player Number"
              name="player_number"
              value={this.state.player_number}
              onChange={this.handleInputChange}
            />
            <Form.Control
              style={{ margin: "8px 0px" }}
              size="sm"
              type="text"
              placeholder="player Age"
              name="player_age"
              value={this.state.player_age}
              onChange={this.handleInputChange}
            />
            <Form.Control
              style={{ margin: "8px 0px 0px" }}
              size="sm"
              type="text"
              placeholder="player position"
              name="player_type"
              value={this.state.player_type}
              onChange={this.handleInputChange}
            />
            <div
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "flex-start",
              }}
            >
              <Button type="submit" size="sm" variant="primary">
                update player
              </Button>
              <Button variant="light" onClick={this.props.onHide}>
                x
              </Button>
            </div>
          </Form>
        </Card.Body>
      </Card>
    );
  }
}

export default UpdatePlayer;
