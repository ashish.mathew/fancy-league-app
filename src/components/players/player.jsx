import React, { Component } from "react";

import {connect} from "react-redux";
import { Card, Button } from "react-bootstrap";

import UpdatePlayer from "./updatePlayer";
import { updatePlayer } from "../../actions/playerActions";
import "../players/players.css"
class Player extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showUpdate: false,
    };
  }

  handleCloseUpdate = () => this.setState({ showUpdate: false });
  handleShowUpdate = () => this.setState({ showUpdate: true });


  handleUpdatePlayer = (player) => {
    this.props.updatePlayer(player)
    this.handleCloseUpdate()
  }

  render() {
    const { player } = this.props;
    return (
      <div className="grow">
        {this.state.showUpdate ? (
          <UpdatePlayer
            player={player}
            style={{ width: "14rem", height: "14rem" }}
            show={this.state.showUpdate}
            onHide={this.handleCloseUpdate}
            onUpdate={this.handleUpdatePlayer}
          />
        ) : (
          <div>
            <Button
              variant="white"
              style={{ borderRadius: "500px" }}
              onClick={(e) => {
                e.stopPropagation();
                this.props.onDelete(player.id);
              }}
            >
              <img
                src="https://img.icons8.com/flat-round/30/000000/delete-sign.png"
                alt="x"
              />
            </Button>
            <Card
              style={{
                width: "13rem",
                boxShadow: "0 3px 10px rgb(0 0 0 / 0.2)",
              }}
            >
              <Card.Img variant="top" src={`${player.player_image}`} />
              <Card.Body>
                <Card.Title style={{ textAlign: "center" }}>
                  {player.player_name}
                </Card.Title>
                <Card.Text>
                  <ul style={{ listStyleType: "none" }}>
                    <li>Number:{player.player_number}</li>
                    <li>Age:{player.player_age}</li>
                    <li>position:{player.player_type}</li>
                  </ul>
                </Card.Text>
                <Button
                  size="sm"
                  variant="white"
                  onClick={this.handleShowUpdate}
                >
                  update{" "}
                  <img
                    alt=""
                    src="https://img.icons8.com/flat-round/30/000000/loop.png"
                  />
                </Button>
              </Card.Body>
            </Card>
          </div>
        )}
      </div>
    );
  }
}
export default connect(null,{updatePlayer})(Player);
