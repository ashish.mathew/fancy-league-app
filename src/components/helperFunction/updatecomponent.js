export let updateComponent = (player, collections) => {
  return collections.map((collection) => {
    if (collection.id === player.id) {
      return { ...collection, ...player };
    }
    return collection;
  });
};
