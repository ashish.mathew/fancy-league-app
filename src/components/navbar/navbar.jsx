import React from "react";
import { Navbar} from "react-bootstrap";
import { Link } from "react-router-dom";
import "./navbar.css";

const NavBar = () => {
  return (
    <Navbar
      style={{ display: "flex", justifyContent: "center" }}
    >
      <Navbar.Brand href="#home">
        <Link to="/" style={{textDecoration:"none"}}>
          <h1 style={{ textAlign: "center" }}>fancy League</h1>
        </Link>
      </Navbar.Brand>
    </Navbar>
  );
};

export default NavBar;
