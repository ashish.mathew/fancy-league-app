import React from "react";

import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";

import "./team.css";

const Team = (props) => {
  const { team, onDelete } = props;
  return (
  
    <div className="card-container">
        <Button
          variant="white"
          style={{borderRadius:"500px", }}
          onClick={(e) => {
            e.stopPropagation();
            onDelete(team.id);
          }}
        >
          <img
            src="https://img.icons8.com/flat-round/30/000000/delete-sign.png"
            alt="x"
          />
        </Button>
            <Link to={`teams/players/${team.id}`} className="text-decoration-none">
        <div
          style={{
            backgroundImage: `url(${team.team_logo})`,
            backgroundRepeat: "no-repeat",
            minHeight: "13rem",
          }}
        >
          {" "}
        </div>
        <h3>{team.team_name}</h3>
    </Link>
      </div>
  
  );
};

export default Team;
