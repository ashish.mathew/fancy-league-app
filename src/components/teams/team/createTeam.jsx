import React, { Component } from "react";

import { Button, Card, Form } from "react-bootstrap";

class CreateTeam extends Component {
  constructor(props) {
    super(props);

    this.state = {
      teamName: "",
    };
  }

  render() {
    return (
      <Card
        className="mb-2"
        style={{
          display: `${this.props.show}`,
          width: "14rem",
          height: "14rem",
        }}
      >
        <Card.Header>ADD YOUR TEAM</Card.Header>
        <Card.Body>
          <Form
            onSubmit={(e) => {
              e.preventDefault();
              this.props.onCreate(this.state.teamName);
            }}
          >
            <Form.Control
              size="sm"
              type="text"
              placeholder="team name"
              onChange={(e) => this.setState({ teamName: e.target.value })}
            />
            <div
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "flex-start",
              }}
            >
              <Button type="submit" size="sm" variant="primary">
                add team
              </Button>
              <Button variant="light" onClick={this.props.onHide}>
                x
              </Button>
            </div>
          </Form>
        </Card.Body>
      </Card>
    );
  }
}

export default CreateTeam;
