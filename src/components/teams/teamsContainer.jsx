import React, { Component } from "react";

import { connect } from "react-redux";
import { Button } from "react-bootstrap";

import { fetchTeams, addTeam, deleteTeam } from "../../actions/teamActions";
import Team from "./team/team";
import "./teamsContainer.css";
import CreateTeam from "./team/createTeam";
import bgimg from "../../mybg.jpg";

class TeamsContainer extends Component {
  constructor() {
    super();
    this.state = {
      show: false,
    };
  }

  handleClose = () => this.setState({ show: false });
  handleShow = () => this.setState({ show: true });

  componentDidMount() {
    this.props.fetchTeams();
  }

  handleAddTeam = (teamName) => {
    console.log(teamName);
    this.props.addTeam(teamName);
    this.handleClose();
  };
  handleDeleteTeam = (teamId) => {
    this.props.deleteTeam(teamId);
  };

  render() {
    return (
      <div style={{
        backgroundImage: `url(${bgimg})`,
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        backgroundPosition:"center",
        height:"100vh"
      }}>
        <h1 style={{ textAlign: "center" }}>fancy League</h1>
  
          

        <div className="team-list">
          {this.props.teams.map((team) => (
            <Team team={team} key={team.id} onDelete={this.handleDeleteTeam} />
          ))}
          <div className="mx-2 my-5">
            {this.state.show ? (
              <CreateTeam
                style={{ width: "14rem", height: "14rem" }}
                show={this.state.show}
                onHide={this.handleClose}
                onCreate={this.handleAddTeam}
              />
            ) : (
              <Button
                style={{
                  opacity:"0.5",
                  width: "14rem",
                  height: "13rem",
                }}
                variant="light"
                onClick={this.handleShow}
              >
                <h2>add Team</h2>
              </Button>
            )}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  teams: state.teams.teams,
});

export default connect(mapStateToProps, { fetchTeams, addTeam, deleteTeam })(
  TeamsContainer
);
