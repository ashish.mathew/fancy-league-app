import * as actions from "../actions/actionTypes";
import { deleteComponent } from "../components/helperFunction/deleteComponent";

const initialState = {
  teams: [],
};

const TeamsReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.FETCH_TEAMS:
      return {
        ...state,
        teams: action.payload,
      };
    case actions.ADD_TEAM:
      return {
        ...state,
        teams: [...state.teams, action.payload],
      };
      case actions.DELETE_TEAM:
      return {
        ...state,
        teams: deleteComponent(action.payload, state.teams),
      };
    default:
      return state;
  }
};

export default TeamsReducer;
