import { combineReducers } from "redux";
import TeamReducer from "./teamsReducer";
import PlayersReducer from "./playersReducer";

export default combineReducers({
  teams: TeamReducer,
  players: PlayersReducer,
});
