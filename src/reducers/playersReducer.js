import * as actions from "../actions/actionTypes";
import { deleteComponent } from "../components/helperFunction/deleteComponent";


const initialState = {
  players: [],
};

const PlayersReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.FETCH_PLAYERS:
      return {
        ...state,
        players: action.payload,
      };
    case actions.ADD_PLAYER:
      return {
        ...state,
        players: [...state.players, action.payload],
      };
    case actions.DELETE_PLAYER:
      return {
        ...state,
        players: deleteComponent(action.payload, state.players),
      };
    case actions.UPDATE_PLAYER:
      return {
        ...state,
        players: state.players.map(player => { if(player.id === action.payload.id) {
          console.log(action.payload)
          return action.payload
        }
        return player;
          })
      };
    default:
      return state;
  }
};

export default PlayersReducer;
