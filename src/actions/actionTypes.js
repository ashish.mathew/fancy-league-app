//teams actions types

export const FETCH_TEAMS = "FETCH_TEAMS";
export const ADD_TEAM = "ADD_TEAM";
export const DELETE_TEAM = "DELETE_TEAM";


//player
export const FETCH_PLAYERS = "FETCH_PLAYERS";
export const ADD_PLAYER = "ADD_PLAYER";
export const DELETE_PLAYER = "DELETE_PLAYER";
export const UPDATE_PLAYER = "UPDATE_PLAYER";
