import * as actions from "../actions/actionTypes";
import * as apiCalls from "../middleware/api";

export const fetchPlayers = (teamId) => async (dispatch) => {
  let players = await apiCalls.fetchPlayersFromApi(teamId);
  dispatch({
    type: actions.FETCH_PLAYERS,
    payload: players,
  });
};

export const addPlayer = (teamId,newplayer) => async (dispatch) => {
  let player = await apiCalls.addPlayerToApi(teamId,newplayer);
  dispatch({
    type: actions.ADD_PLAYER,
    payload: player,
  });
};

export const deletePlayer = (playerId) => async (dispatch) => {
  await apiCalls.deletePlayerFromApi(playerId);
  dispatch({
    type: actions.DELETE_PLAYER,
    payload: playerId,
  });
};

export const updatePlayer = (player) => async (dispatch) => {
  await apiCalls.updatePlayerFromApi(player);
  dispatch({
    type: actions.UPDATE_PLAYER,
    payload: player,
  });
};