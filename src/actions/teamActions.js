import * as actions from "../actions/actionTypes";
import * as apiCalls from "../middleware/api";

export const fetchTeams = () => async (dispatch) => {
  let teams = await apiCalls.fetchTeamsFromApi();
  dispatch({
    type: actions.FETCH_TEAMS,
    payload: teams,
  });
};

export const addTeam = (TeamName) => (dispatch) => {
  console.log(TeamName);
  apiCalls.addNewTeamToApi(TeamName).then((newTeam) =>
    dispatch({
      type: actions.ADD_TEAM,
      payload: newTeam,
    })
  );
};

export const deleteTeam = (teamId) => async (dispatch) => {
  await apiCalls.deleteTeamFromApi(teamId);
  dispatch({
    type: actions.DELETE_TEAM,
    payload: teamId,
  });
};
