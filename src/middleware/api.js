export const fetchTeamsFromApi = async () => {
  let Teams = await fetch(`https://fancy11.herokuapp.com/api/teams`).then(
    (response) => response.json()
  );
  return Teams;
};

export const addNewTeamToApi = async (TeamName) => {
  let newTeam = await fetch(`https://fancy11.herokuapp.com/api/teams`, {
    method: "POST",
    body: JSON.stringify({
      team_name: TeamName,
    }),
    headers: { "Content-Type": "application/json" },
  }).then((response) => response.json());
  return newTeam;
};

export const deleteTeamFromApi = async (teamId) => {
  let id = await fetch(`https://fancy11.herokuapp.com/api/teams/${teamId}`, {
    method: "DELETE",
  }).then((response) => response.json());
  return parseInt(id);
};

export const fetchPlayersFromApi = async (teamId) => {
  let players = await fetch(
    `https://fancy11.herokuapp.com/api/teams/players/${teamId}`
  ).then((response) => response.json());
  return players;
};

export const addPlayerToApi = async (teamId, newPlayer) => {
  const { playerName, playerAge, playerNumber, playerType } = newPlayer;
  let player = await fetch(
    `https://fancy11.herokuapp.com/api/teams/players/${teamId}`,
    {
      method: "POST",
      body: JSON.stringify({
        player_name: playerName,
        player_age: playerAge,
        player_number: playerNumber,
        player_type: playerType,
      }),
      headers: { "Content-Type": "application/json" },
    }
  ).then((response) => response.json());
  return player;
};

export const deletePlayerFromApi = async (playerId) => {
  let id = await fetch(
    `https://fancy11.herokuapp.com/api/teams/players/${playerId}`,
    {
      method: "DELETE",
    }
  ).then((response) => response.json());
  return parseInt(id);
};

export const updatePlayerFromApi = async (player) => {
  const { player_name, player_age, player_number, player_type, id } = player;
  let updatedplayer = await fetch(
    `https://fancy11.herokuapp.com/api/teams/players/${id}`,
    {
      method: "PUT",
      body: JSON.stringify({
        player_name: player_name,
        player_age: player_age,
        player_number: player_number,
        player_type: player_type,
      }),
      headers: { "Content-Type": "application/json" },
    }
  ).then((response) => response.json());
  return updatedplayer;
};
